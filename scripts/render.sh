#!/usr/bin/env sh

${HOME}/go/bin/render --in "${PAPER_NAME}.md.tmpl" --out "${PAPER_NAME}.md" --config "${PAPER_NAME}.yaml"

