#!/usr/bin/env sh

yq e '.profile.name.first, .profile.name.last' "${PAPER_NAME}.yaml" | tr '\n' '-'

