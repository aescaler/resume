#!/usr/bin/env sh

TEMPLATE_LATEX="resume-resources/resume-pandoc/templates/jb2resume.latex"

pandoc \
  -f markdown+yaml_metadata_block \
--template "${TEMPLATE_LATEX}" \
  -s "${PAPER_NAME}.md" \
  -o "${PAPER_NAME}.pdf"

cp "${PAPER_NAME}.pdf" "${PAPER_AUTHOR_NAME%?}-${PAPER_NAME}.pdf"

