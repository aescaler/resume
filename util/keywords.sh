#!/usr/bin/env bash

DATA=resume.yaml
KEYWORDS=keywords.txt

cp "${DATA}" "${KEYWORDS}"
sed -i.bak 's/- //g' "${KEYWORDS}"
sed -i.bak -E 's/ *[a-z]+://g' "${KEYWORDS}"
sed -i.bak 's/[ \n]/\n/g' "${KEYWORDS}"
sed -i.bak 's/[,.]$//g' "${KEYWORDS}"
sed -i.bak -E 's;[^a-zA-z0-9/\.\n@\-]*;;g' "${KEYWORDS}"
sort "${KEYWORDS}" > "${KEYWORDS}.bak"
uniq "${KEYWORDS}.bak" > "${KEYWORDS}"

rm "${KEYWORDS}.bak"

