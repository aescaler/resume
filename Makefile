PAPER_NAME ?= resume
PAPER_CONTAINER_INTERFACE ?= podman
PAPER_CONTAINER_IMAGE_GO ?= cgr.dev/chainguard/go@sha256:b5a3dc1b4052ff24e844360ef661378d584bbaf6d55a80350115bc6a955ae207
PAPER_CONTAINER_IMAGE_YQ ?= docker.io/mikefarah/yq@sha256:877e34d86fe527e5fee9353356387f3ecbc13a639cf3fe0f3f3cd3ac3f169929
PAPER_CONTAINER_IMAGE_LATEX ?= docker.io/pandoc/latex@sha256:bea599351bb3ccaf33d5edd22a243b5e72b9c72674b4d7e180f2a9fdce48db08
PAPER_CONTAINER_IMAGE_RENDER_CUSTOM ?= paper.local/golang/render:latest
PAPER_CONTAINER_IMAGE_LATEX_CUSTOM ?= paper.local/pandoc/latex:latest

.PHONY: all render pdf clean container_image/*

all: render pdf clean

render: container_image/render
	$(PAPER_CONTAINER_INTERFACE) \
		run --rm -it \
		--entrypoint="/bin/sh" \
		-e PAPER_NAME="$(PAPER_NAME)" \
		-v "${PWD}:/data" \
		-w "/data" \
		$(PAPER_CONTAINER_IMAGE_RENDER_CUSTOM) \
		-- ./scripts/render.sh

pdf: PAPER_AUTHOR_NAME = $(shell $(PAPER_CONTAINER_INTERFACE) run --rm -it --entrypoint="/bin/sh" -e PAPER_NAME="$(PAPER_NAME)" -v "${PWD}:/data" -w "/data" $(PAPER_CONTAINER_IMAGE_YQ) -- ./scripts/author.sh)
pdf: container_image/latex
	$(PAPER_CONTAINER_INTERFACE) \
		run --rm -it \
		--entrypoint="/bin/sh" \
		-e PAPER_NAME="$(PAPER_NAME)" \
		-e PAPER_AUTHOR_NAME="$(PAPER_AUTHOR_NAME)" \
		-v "${PWD}:/data" \
		-w "/data" \
		$(PAPER_CONTAINER_IMAGE_LATEX_CUSTOM) \
		-- ./scripts/pdf.sh

clean:
	rm "${PAPER_NAME}.md" "${PAPER_NAME}.pdf" || true

container_image/render:
	# $(PAPER_CONTAINER_INTERFACE) pull $(PAPER_CONTAINER_IMAGE_GO)
	$(PAPER_CONTAINER_INTERFACE) build \
		--build-arg BASE_IMAGE=$(PAPER_CONTAINER_IMAGE_GO) \
		-f ./containers/Containerfile.render \
		./containers/ \
		-t $(PAPER_CONTAINER_IMAGE_RENDER_CUSTOM)

container_image/latex:
	# $(PAPER_CONTAINER_INTERFACE) pull $(PAPER_CONTAINER_IMAGE_LATEX)
	$(PAPER_CONTAINER_INTERFACE) build \
		--build-arg BASE_IMAGE=$(PAPER_CONTAINER_IMAGE_LATEX) \
		-f ./containers/Containerfile.pandoc \
		./containers/ \
		-t $(PAPER_CONTAINER_IMAGE_LATEX_CUSTOM)
